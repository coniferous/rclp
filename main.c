/* RCLib, utility program for RC stuff */
/* rue_mohr, polprog */

#include "RClib.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

//TODO: Clean up code, add functionality to pick C or L given f and L or C

void printhelp(){
  printf(
	 "RClib version " __VERSION "\n"
	 "A small command line utility for calculating various RLC\n"
	 "circuit parameters\n"
	 "\n"
	 "Depending on the values given, it calculates different stuff\n\n"
	 "Usage: "
	 "rclib [-r resistance] [-c capacitance] [-f freq] [-v voltage] [-l inductance]\n"
	 "Options aren't case sensitive. SI prefixes are. Use u for micro\n"
	 "SI prefixes are supported: 12k 4.7u .33M\n"

	 );

}



int main(int argc, char** argv) {
  bool done_thing = false;
  RCLP_t n = {NAN, NAN, NAN, NAN, NAN};
  double freq = NAN;
  int opt;
  extern char *optarg;
  extern int optind, opterr, optopt;
  while((opt = getopt(argc, argv, "r:c:R:C:v:V:f:F:l:L")) != -1){
    switch(opt){
    case 'c':
    case 'C':
      n.C = scansci(optarg);
      break;
    case 'r':
    case 'R':
      n.R = scansci(optarg);
      break;
    case 'v':
    case 'V':
      n.E = scansci(optarg);
      break;
    case 'f':
    case 'F':
      freq = scansci(optarg);
      break;
    case 'l':
    case 'L':
      n.L = scansci(optarg);
    }
  }
  if(isnan(freq) && isnan(n.R) && isnan(n.C) && isnan(n.E) && isnan(n.L)){
    printhelp();
    return 0;
  }

  if(!isnan(freq) && isnan(n.R) && isnan(n.C) && isnan(n.E) && isnan(n.L)){
    printf("You feel a tingling sensation of a ");
    printsci_u(freq, "Hz signal...\n");
    done_thing=true;
  }

  //Given f, solve for best filter.
  if(!isnan(freq) && isnan(n.R) && isnan(n.C) && isnan(n.E) && isnan(n.L)){
    printf("First order RC filter\n");
    RCLP_t fil = solve_filter(&n, freq);
    printf("Best fit:\n");
    printsci_u(fil.R, "ohm\t");
    printsci_u(fil.C, "F\n");
    printsci_u(knee_freq(fil.R, fil.C), "Hz\t");
    printf("%+06.04lf%% error\n", (knee_freq(fil.R, fil.C)-freq)*100/freq);
    done_thing=true;

  }

  //Given f and C or L, find impedance
  if(!isnan(n.C) && !isnan(freq) && isnan(n.R) && isnan(n.L)){
    printf("Impedance is:\t");
    printsci_u(1/(2*M_PI*freq*n.C), "ohmj\n");
    done_thing=true;

  }
  if(!isnan(n.L) && !isnan(freq) && isnan(n.R) && isnan(n.C)){
    printf("Impedance is:\t");
    printsci_u(2*M_PI*freq*n.L, "ohmj\n");
    done_thing=true;
  }

  //Given C, L. Find LC resonance
  if(!isnan(n.L) && !isnan(n.C) && isnan(freq)){
    printf("Tuned circuit resonance freq:\t");
    printsci_u(1/(2*M_PI*sqrt(n.L * n.C)), "Hz\n");
    done_thing=true;
  }

  //If frequency is also given, find impedance
  if(!isnan(n.L) && !isnan(n.C) && !isnan(freq)){
    printf("Paralell impedance:\t");
    printsci_u(parallel_impedance(2*M_PI*freq*n.L, 1/(2*M_PI*freq*n.C)), "ohmj\n");
    printf("Series impedance:\t");
    printsci_u(series_impedance(2*M_PI*freq*n.L, 1/(2*M_PI*freq*n.C)), "ohmj\n");
    done_thing=true;
  }

  //Given RCL, no f
  if(!isnan(n.L) && !isnan(n.C) && isnan(freq) && !isnan(n.R)){
    double fc = 1/(2*M_PI*sqrt(n.L * n.C));
    printf("Series RLC, Q:\t\t");
    printsci(1/n.R * sqrt(n.L/n.C));
    printf("\n Bandwidth:\t\t");
    printsci_u(fc/(1/n.R * sqrt(n.L/n.C)),"Hz\n");
    printf("Parallel RLC, Q:\t");
    printsci(n.R * sqrt(n.C/n.L));
    printf("\n Bandwidth:\t\t");
    printsci_u(fc/(n.R * sqrt(n.C/n.L)), "Hz\n");

    done_thing=true;
  }
  //Given RCL and f
  if(!isnan(n.L) && !isnan(n.C) && !isnan(freq) && !isnan(n.R)){

    done_thing=true;
  }




  //Given R and f, find C (solve RC filter)
  if(!isnan(n.R) && !isnan(freq) && isnan(n.C)){
    printf("First order RC filter\n");
    RCLP_t x = solve_filter_R(closest_E24(n.R), freq);
    printsci_u(x.R, "ohm\t");
    printsci_u(x.C, "F\n");
    printsci_u(knee_freq(x.R, x.C), "Hz\t");
    printf("%+06.04lf%% error\n", (knee_freq(x.R, x.C)-freq)*100/freq);
    done_thing=true;
  }

  //Given C and E
  if(isnan(n.R) && !isnan(n.C) && !isnan(n.E)){
    printf("Energy stored:\t");
    printsci_u(n.C * n.E * n.E / 2, "J\n");
    done_thing=true;
  }

  //Given R and C, print time constant stuff
  if(!isnan(n.R) && !isnan(n.C) && isnan(n.L)){
    printf("RC time constant (tau)=\t");
    printsci_u(n.R * n.C, "s\n");
    printf("1st order knee freq =\t");
    printsci_u(knee_freq(n.R, n.C), "Hz\n");
    printf("kT/C noise = ");
    printsci_u(nyquist_noiseRC(n.C, 300), "VRMS @ room temp\n");

    printf("Rises:\t\t50%%\t\t66.3..%%\t\t99%%\t\t99.9%%\n\t");
    printsci_u(charge_time(1, n.R, n.C, 0.5), "s\t");
    printsci_u(charge_time(1, n.R, n.C, (1-exp(-1))), "s\t");
    printsci_u(charge_time(1, n.R, n.C, 0.99), "s\t");
    printsci_u(charge_time(1, n.R, n.C, 0.999), "s\t\n");
    done_thing=true;

  }

  //If frequency is also given, print attenuation.
  if(!isnan(n.R) && !isnan(freq) && !isnan(n.C)){
    printf("Attenuations (1st order):\nLowpass: \t");
    printsci(lowpass_attn(n.R, n.C, freq));
    printf("\t%06.04lf dB\t", ratio_to_db(lowpass_attn(n.R, n.C, freq)));
    if(!isnan(n.E)) printsci_u(lowpass_attn(n.R, n.C, freq)*n.E, "Vamp");

    printf("\nHighpass: \t");
    printsci(highpass_attn(n.R, n.C, freq));
    printf("\t%06.04lf dB\t", ratio_to_db(highpass_attn(n.R, n.C, freq)));
    if(!isnan(n.E)) printsci_u(highpass_attn(n.R, n.C, freq)*n.E, "Vamp");
    printf("\n");
    done_thing=true;
  }

  //Givenn R and E, Ohms law
  if(!isnan(n.R) && !isnan(n.E) && isnan(n.C)){
    printf("I = ");
    printsci_u(n.E / n.R, "A\t");
    printf("P = ");
    printsci_u(n.E * n.E / n.R, "W\n");
    done_thing=true;
  }

  //Fallbacks for useless data
  if(!done_thing){
    printf("A hollow voice says: \"You fool\"\n");
  }

  if(isnan(freq) && !isnan(n.R) && isnan(n.C) && isnan(n.E) && isnan(n.L)){
    printf("There is a ");
    printsci_u(n.R, "ohm resistor here.\n");
  }
  if(isnan(freq) && isnan(n.R) && !isnan(n.C) && isnan(n.E) && isnan(n.L)){
    printf("There is a ");
    printsci_u(n.C, "F cap here.\n");
  }
  if(isnan(freq) && isnan(n.R) && isnan(n.C) && !isnan(n.E) && isnan(n.L)){
    printf("There is a ");
    printsci_u(n.E, "V source here.\n");
  }

  if(isnan(freq) && isnan(n.R) && isnan(n.C) && !isnan(n.L) && isnan(n.E)){
    printf("There is a ");
    printsci_u(n.L, "H inductor here.\n");
  }
  return 0;
}
