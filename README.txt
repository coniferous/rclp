RCLP
A handy CLI tool for calculating RC filters

Give it any set of parameters and it will try to calculate useful figures

Usage: rclib [-v voltage] [-f freq] [-r resistance] [-c capacitance]
Accepts SI prefixes, optins are case insensitive. Use u for micro


Sample outputs:

 <+> Calculate knee freq, rise times and attenuations (-v can be omitted)

$ rclib -v 15 -r 10k -c 10n -f 33k
RC time constant (tau)= 100.000000 us
1st order knee freq =     1.591549 kHz
kT/C noise = 643.579599 nVRMS @ room temp
Rises:          50%             66.3..%         99%             99.9%
         69.314718 us   100.000000 us   460.517019 us   690.775528 us
 Attenuations (1st order):
	 Lowpass:         48.172778 m    -26.3440 dB     722.591668 mVamp
	 Highpass:       998.839018 m    -0.0101 dB       14.982585  Vamp


 <+> Pick a filter from standard values: 

$ rclib -f 3.14k -r 330
First order RC filter
330.000000  ohm 150.000000 nF
  3.215251 kHz  +2.3965% error

  ...or...


$ rclib -f 3.14k
You feel a tingling sensation of a   3.140000 kHz signal...
First order RC filter
Looking for standard value pairs that give a freq of   3.140000 kHz
        R               C       f3db            error%
--------------------------------------------------------
  6.200000 kohm   8.200000 nF     3.130506 kHz  -0.3023%
  5.100000 kohm  10.000000 nF     3.120685 kHz  -0.6151%
  7.500000 kohm   6.800000 nF     3.120685 kHz  -0.6151%
  1.100000 kohm  47.000000 nF     3.078432 kHz  -1.9608%
  1.600000 kohm  33.000000 nF     3.014298 kHz  -4.0032%
  2.400000 kohm  22.000000 nF     3.014298 kHz  -4.0032%
  2.000000 kohm  27.000000 nF     2.947314 kHz  -6.1365%
  3.600000 kohm  15.000000 nF     2.947314 kHz  -6.1365%
  1.000000 kohm  47.000000 nF     3.386275 kHz  +7.8432%
  6.800000 kohm   8.200000 nF     2.854285 kHz  -9.0992%
  8.200000 kohm   6.800000 nF     2.854285 kHz  -9.0992%
  5.600000 kohm  10.000000 nF     2.842053 kHz  -9.4888%
Best fit:
  6.200000 kohm   8.200000 nF
  3.130506 kHz  -0.3023% error


  <+> Various other little bits

$ rclib -f 3.14k -c 15u
Impedance is:     3.379086  ohmj
$ rclib -r 10k -v 10
I =   1.000000 mA       P =  10.000000 mW
$ rclib -v 10k -c 15u
Energy stored:  750.000000  J

