/* RCLib, utility program for RC stuff */
/* rue_mohr, polprog */

#ifndef __rclib
#define __rclib


#include <math.h>
#include <complex.h>

#ifndef M_PI
#define M_PI (3.14159265358979323846264338327950288)
#endif /* M_PI */


typedef struct RCLP_s {

  double 
     E  // applied voltage
    ,C  // capacitance in farads
    ,R  // resistance in ohms
    ,V0  // capacitor voltage
    ,L //Inductance in henries
    ;  
} RCLP_t;

typedef struct {
  RCLP_t params;
  double freq;
  
} filter_t;


typedef struct E24R {
  int index; int exponent;
} E24R_t;

void timeStep( RCLP_t * this, double t);

double scansci(char* str);
void printsci(double d);
void printsci_u(double d, char* unit);


double reactance(double c, double f);
double lowpass_attn(double r, double c, double f);
double highpass_attn(double r, double c, double f);
double ratio_to_db(double x);
double knee_freq(double r, double c);
double charge_time(double e, double r, double c, double v);
double discharge_time(double e, double r, double c, double v);
double nyquist_noiseRC(double c, double T);
double nyquist_noiseR(double r, double T, double bw);

double complex parallel_impedance(double complex za, double complex zb);
double complex series_impedance(double complex za, double complex zb);



int filter_compare(const void* a, const void* b);
RCLP_t solve_filter(RCLP_t *source, double f);
RCLP_t solve_filter_R(E24R_t r, double f);

E24R_t E24_step(E24R_t r, int step);
E24R_t closest_E24(double r);
E24R_t closest_E12(double x);
double E24_to_R(E24R_t x);

#endif
