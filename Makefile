CC=gcc

# for manual installation
INSTALLDIR=/usr/local/bin

# .deb package stuff
VERSION := 1.0
GITHASH = $(shell git log -1 --format="%h" || echo "unknown")
PKGNAME = rclib
PKGVERSION := $(VERSION)+git-1
PKGDIR := $(PKGNAME)_$(PKGVERSION)
PREFIX ?= /usr
PKGARCH = amd64


CFLAGS += -g -Wall -Wextra -D__VERSION=\"$(VERSION)-$(GITHASH)\"
LDFLAGS += -lm 
SOURCES = RClib.c main.c
OBJ = $(SOURCES:%.c=%.o)


all: rclib


rclib: $(OBJ)
	$(CC) $(CFLAGS) $^ $(LDFLAGS) -o $@


%.o: %.cpp 
	$(CC) $(CFLAGS) -c -o $@ $< 
deb: rclib
	mkdir -p $(PKGDIR)$(PREFIX)/bin
	cp rclib $(PKGDIR)$(PREFIX)/bin
	mkdir -p $(PKGDIR)/DEBIAN/
	cp package_meta.txt $(PKGDIR)/DEBIAN/control
	sed -i -e "s/%VERSION%/$(PKGVERSION)/" $(PKGDIR)/DEBIAN/control
	sed -i -e "s/%ARCH%/$(PKGARCH)/" $(PKGDIR)/DEBIAN/control
	@echo "Building the debian package..."
	dpkg-deb --build $(PKGDIR)


clean:
	rm -rf *.o $(PKGDIR)

install:
	install rclib $(INSTALLDIR) 
