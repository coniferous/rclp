/* RCLib, utility program for RC stuff */
/* rue_mohr, polprog */

#include "RClib.h"
#include <stdio.h>
#include <stdlib.h>


const char prefixes[] = {'G', 'M', 'k', ' ', 'm', 'u', 'n', 'p', 'f'};
const double kb = 1.380649E-23;

#define E24_LEN 24
const double e24[] = {1.0, 1.1, 1.2, 1.3, 1.5, 1.6, 1.8, 2.0, 2.2,
		      2.4, 2.7, 3.0, 3.3, 3.6, 3.9, 4.3, 4.7, 5.1,
		      5.6, 6.2, 6.8, 7.5, 8.2, 9.1};
  


void timeStep( RCLP_t * this, double t) {
 // there is a more unified version of this calculation I can quite muster in my head.
  if (this->E > this->V0) {
    this->V0 = this->V0 + (this->E - this->V0) * (1.0 - exp( -(t /(this->R*this->C))) );
  } else {
    this->V0 = this->E + (this->V0 - this->E) * (exp( -(t /(this->R*this->C))) );
  }
}

double scansci(char* str){
  double d = atof(str);
  char* x = str;
  for(; (*x >= '0' && *x <= '9') || *x == '.'; x++);
  switch(*x){
  case 'f':
    d /= 10000000000000000; break;
  case 'p': 
    d /= 1000000000000; break;
  case 'n':
    d /= 1000000000; break; 
  case 'u':
    d /= 1000000; break; 
  case 'm':
    d /= 1000; break;
  case 'k': 
    d *= 1000; break;
  case 'M':
    d *= 1000000; break;
  case 'G':
    d *= 1000000000; break;
  }
}

void printsci(double d){
  printsci_u(d, "");
}

void printsci_u(double d, char* unit) {
  int exponent = 3; //no exponent is prefixes[3]

  if(d != 0 && !isinf(d) && !isnan(d)){ 
    while(d >= 1000 && exponent > 0){
      d /= 1000;
      exponent--;
    }
    while(d < 1 && exponent < 8){
      d *= 1000;
      exponent++;
    }
  }
  printf("%10.6lf %c%s", d, prefixes[exponent], unit);
  
}

/* Calculation functions */

double knee_freq(double r, double c){
  return 1/(2*M_PI*r*c);
}

double charge_time(double e, double r, double c, double v){
  return -r * c * log((e-v)/e);
  
}

double discharge_time(double e, double r, double c, double v){
  return 0;
  
}

double reactance(double c, double f){
  return 1/(2*M_PI*f*c);
}

double lowpass_attn(double r, double c, double f){
  double xc = reactance(c, f);
  return xc/sqrt(xc*xc + r*r);
}

double highpass_attn(double r, double c, double f){
  double xc = reactance(c, f);
  return r/sqrt(xc*xc + r*r);
}

double ratio_to_db(double x){
  return 20*log10(x);
}

/* 
   return the kT/C noise. The broadband noise when the input is 
   shorted to gnd
*/
double nyquist_noiseRC(double c, double T){
  return sqrt(kb*T/c);
}

/* Nyquist noise RMS for a resistor */
double nyquist_noiseR(double r, double T, double bw){
  return sqrt(4*kb*T*r*bw);
}


int filter_compare(const void *a, const void *b){
  filter_t _a = *(filter_t*)a;
  filter_t _b = *(filter_t*)b;
  double err1, err2;
  err1 = fabs(knee_freq(_a.params.R, _a.params.C) - _a.freq);
  err2 = fabs(knee_freq(_b.params.R, _b.params.C) - _b.freq);
  
  if(err1 < err2) return -1;
  else if (err1 == err2) return 0;
  else return 1;
}

/* Wrapper function for solving filters.
 * If only F is given, finds the caps for all E24 resistors in the
 * single kohm range. Prints all the soltions that have <10% error
 * If both F and R are given, finds the nearest E12 cap.
 */
RCLP_t solve_filter(RCLP_t *source, double f){
  if(isnan(source->R) && isnan(source->C)){
    printf("Looking for standard value pairs that give a freq of ");
    printsci_u(f, "Hz\n");
    filter_t possibilities[E24_LEN];
    for(int i = 0; i < E24_LEN; i++) {
      
      E24R_t r = {i, 3};
      filter_t filter;
      filter.params = solve_filter_R(r, f);
      filter.freq = f;
      possibilities[i] = filter;
      
    }
    qsort(possibilities, E24_LEN, sizeof(filter_t), filter_compare);
    printf("\tR\t\tC\tf3db\t\terror%%\n");
    printf("--------------------------------------------------------\n");
    for(int i = 0; i < E24_LEN; i++) {
      double error = (knee_freq(possibilities[i].params.R,
				possibilities[i].params.C)-f)*100/f;
      if(fabs(error) > 10) break;

      printsci_u(possibilities[i].params.R, "ohm\t");
      printsci_u(possibilities[i].params.C, "F\t");
      printsci_u(knee_freq(possibilities[i].params.R,
			   possibilities[i].params.C), "Hz\t");
      printf("%+06.04lf%%", error);
      printf("\n");
      
    }
    return possibilities[0].params;
  }
  if(!isnan(source->R)){
    E24R_t r = closest_E24(source->R);
    return solve_filter_R(r, f);
  }
}

/* Finds nearest E12 cap for a given R and target knee frequency */
RCLP_t solve_filter_R(E24R_t r, double f){
  RCLP_t n;
  
  E24R_t cap = closest_E12(1/(2*M_PI*f*E24_to_R(r)));
  double error = (knee_freq(E24_to_R(r), E24_to_R(cap)) - f)/f;
 
  double last_error = INFINITY;
  double errdiff;
  double last_errdiff = INFINITY;
      
  do{
    n.R = E24_to_R(r);
    n.C = E24_to_R(cap);
    
    last_error = error;
    last_errdiff = errdiff;

    cap = E24_step(cap, error > 0 ? -2 : 2); //caps are E12 so we step two steps

    error = (knee_freq(E24_to_R(r), E24_to_R(cap)) - f)/f;
    errdiff = fabs(last_error - error);

  }while(errdiff < last_errdiff);
  
  
  return n;
}




E24R_t closest_E24(double r){
  E24R_t x;
  x.exponent = floor(log10(r));
  x.index = 0;
  while(r > E24_to_R(x) && x.index < E24_LEN-1){
    x.index++;
  }
  if(x.index > 0 &&
     e24[x.index]*pow(10,x.exponent) - r >
     r - e24[x.index-1]*pow(10,x.exponent)
     ){
    //we've overshot
    x.index --;
    return x;
  }
  //edge case for when x.index == 0, then we need to go down with the
  //exponent and pick the last E24 val
  if(x.index == 0 &&
     r - e24[x.index]*pow(10,x.exponent) <
     e24[E24_LEN-1]*pow(10,x.exponent-1) -r){
       x.index = E24_LEN-1;
       x.exponent --;
       return x;
     }
  //edge case for when x.index is the last value but r is higher than
  //  that (ex. 999ohm -> 1k)

  if(x.index == E24_LEN-1 &&
     r - E24_to_R(x) > e24[0]*pow(10, x.exponent+1) - r){
    x.index = 0;
    x.exponent ++;
  }
  
  return x;
}

E24R_t closest_E12(double x){
  E24R_t z = closest_E24(x);
  if(z.index % 2 != 0){ //didnt hit an E12 value
    if(E24_to_R(E24_step(z, -1)) - x > x - E24_to_R(E24_step(z, 1))){
      z = E24_step(z, 1);
    }else{
      z = E24_step(z, -1);
    }
  }
  return z;
}


double E24_to_R(E24R_t x){
  return e24[x.index]*pow(10,x.exponent);
}


E24R_t E24_step(E24R_t r, int step){
  if(step == 0) return r;
  int direction = step > 0 ? -1 : 1;
  r.exponent += step/E24_LEN;
  step += direction*(step/E24_LEN)*E24_LEN;

  while(step != 0){
    r.index += direction;
    step += direction;
    if(r.index < 0){
      r.index = E24_LEN - 1;
      r.exponent--;
      continue;
    }
    if(r.index > E24_LEN-1){
      r.index = 0;
      r.exponent++;
      continue;
    }
  }
  return r;
}
